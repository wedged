name:             wedged
version:          4
synopsis:         Wedged postcard generator.
license:          OtherLicense
license-file:     LICENSE.md
author:           Claude Heiland-Allen
maintainer:       claude@mathr.co.uk
copyright:        (C) 2013,2015,2016,2018,2023 Claude Heiland-Allen
homepage:         https://mathr.co.uk/wedged
category:         Demo
build-type:       Simple
cabal-version:    >=1.10

description:
    Wedged (C) 2013,2015,2016,2018,2023 Claude Heiland-Allen.
    .
    Copyleft: This is a free work, you can copy, distribute, and
    modify it under the terms of the Free Art License
    <http://artlibre.org/licence/lal/en/>.
    .
    Usage:
    .
    > mkdir 7x5
    > cd 7x5
    > wedged 13 9 0.5 72
    > cd ..
    .
    > mkdir 12x8
    > cd 12x8
    > wedged 14 9 0.8 72
    > cd ..
    .
    Usage with Hugs:
    .
    > mkdir 7x5
    > cd 7x5
    > runhugs -98 -P:../hugs -h1G ../Wedged.hs 13 9 0.5 72
    > cd ..
    .
    Output:
    .
    189 SVG files in the 7x5 dir, totalling 4.5 MB, runtime 4m27s.
    .
    115 SVG files in the 12x8 dir, totalling 2.7 MB, runtime 9m10s.
    .
    Run time measured using a single core of a 4.3GHz AMD Ryzen 7 2700X
    Eight-Core Processor.
    .
    Information:
    .
    Version 0 worked with GHC 7.8 and Diagrams 1.2 with the Cairo backend.
    .
    Version 1 was updated to work with GHC 8.0 and Diagrams 1.3 with the
    Cairo backend.
    .
    Version 2 was updated to work with GHC 8.4 and Diagrams 1.4 with the
    Rasterific backend.
    .
    Version 3 is updated to work with GHC 8.6 and Diagrams 1.4 with the
    Postscript backend.
    .
    Version 4 is updated to work with GHC 8.0 through 9.4,
    with fewer dependencies.
    Hugs compatible, but fails at runtime with an arithmetic overflow.

executable wedged
  main-is:          Wedged.hs
  other-modules:    Compat
  if impl(ghc)
    hs-source-dirs: . ghc
  if impl(hugs)
    hs-source-dirs: . hugs
  build-depends:    base >=4.7 && <4.18,
                    array >=0.5 && <0.6,
                    containers >=0.5 && <0.7,
                    random >=1.1 && <1.3
  default-language: Haskell2010

source-repository head
  type: git
  location: https://code.mathr.co.uk/wedged.git

source-repository this
  type: git
  location: https://code.mathr.co.uk/wedged.git
  tag: v4
